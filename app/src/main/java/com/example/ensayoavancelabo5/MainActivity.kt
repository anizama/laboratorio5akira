package com.example.ensayoavancelabo5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),IListas {
    var vehiculoelegido = ""
    var contador = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tipovehiculo = listOf("Car1", "Car2","Car3","Car4","Car5","Car6")
        val imagenescarros = intArrayOf(R.drawable.car1, R.drawable.car2, R.drawable.car3, R.drawable.car4, R.drawable.car5, R.drawable.car6)

        //val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, languages)
        val adapter = ArrayAdapter(this, R.layout.style_spinner,tipovehiculo)
        spLenguajes.adapter = adapter

        spLenguajes.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                //Toast.makeText(this@MainActivity, languages[position], Toast.LENGTH_SHORT).show()

                if (contador!=0){
                    createLoginDialogo(tipovehiculo[position]).show()
                }
                contador++

            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }

        btnVerificar.setOnClickListener {

            if (vehiculoelegido.isEmpty()){
                Toast.makeText(this,"Debe elegir un vehiculo", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val nombrepropietario = edtNombres.text.toString()
            val aniovehiculo = edtDocumento.text.toString()

            val persona = Propietario(nombrepropietario,aniovehiculo,vehiculoelegido)

            tvResultado.text = "Estimado ${persona.nombre}. Su vehiculo  ${persona.vehiculo}. Corrobore su año de fabricacion del vehiculo " +
                    "identidad que tenemos registrado ${persona.documento}"
        }

    }

    override fun createLoginDialogo(tipovehiculo:String): AlertDialog {
        val alertDialog: AlertDialog
        val builder  = AlertDialog.Builder(this)
        val inflater = layoutInflater
        val v: View = inflater.inflate(R.layout.dialogo, null)
        builder.setView(v)
        val btnAperturarNo: Button = v.findViewById(R.id.btn_aperturar_no)
        val btnAperturarSi: Button = v.findViewById(R.id.txtitem)
        val imgLogo: ImageView = v.findViewById(R.id.imgLogo)

        if (vehiculoelegido == "Car1"){
            imgLogo.setBackgroundResource(R.drawable.car1)
        }
        if (vehiculoelegido=="Car2") {
            imgLogo.setBackgroundResource(R.drawable.car2)
        }
         if (vehiculoelegido=="Car3") {
            imgLogo.setBackgroundResource(R.drawable.car3)
        }
         if (vehiculoelegido=="Car4") {
            imgLogo.setBackgroundResource(R.drawable.car4)
        }
         if (vehiculoelegido=="Car5") {
            imgLogo.setBackgroundResource(R.drawable.car5)
        }
         if (vehiculoelegido=="Car6") {
            imgLogo.setBackgroundResource(R.drawable.car6)
        }

        //crear pop up
        alertDialog = builder.create()

        btnAperturarSi.setOnClickListener {
            vehiculoelegido = tipovehiculo
            Toast.makeText(this,"Has seleccionado $tipovehiculo", Toast.LENGTH_SHORT).show()
            alertDialog.dismiss()
        }

        btnAperturarNo.setOnClickListener{
            alertDialog.dismiss()
        }

        return alertDialog
    }


    }
