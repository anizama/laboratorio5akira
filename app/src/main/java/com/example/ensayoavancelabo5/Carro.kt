package com.example.ensayoavancelabo5

open class Carro(var Modelo:String,var Anio:Int) {

    //Contructores
    constructor(Anio: Int):this("",Anio)
    constructor(Modelo: String):this(Modelo,0)

    open fun cierre(){
        println("El $Modelo que tiene $Anio años se aperturo un año despues")
    }

      fun dormir(){
        println("El $Modelo que tiene $Anio años esta sin stock")
    }

}
