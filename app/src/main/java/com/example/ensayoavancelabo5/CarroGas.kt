package com.example.ensayoavancelabo5

class CarroGas(val marca:String) : Carro(Modelo = "",Anio = 0) {

    fun ladrar(){
        println("El perro de raza $marca esta ladrando")
    }

    override fun cierre() {
        println("El $Modelo que tiene $Anio patas esta comiendo PROPIO")
    }
}